<!--
SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
SPDX-License-Identifier: AGPL-3.0-or-later
-->

# F-Droid Monitor

A simple web-app for browsing fdroid build server infos.

🛠👷🏗 This project is under development, expect rough edges and changes.

## bootstrap dev setup

Here's how you can get a dev setup bootstrapped and ready to develop on your system.

```
git clone https://gitlab.com/fdroid/fdroid-monitor
python3 -m venv env
. env/bin/activate
python3 -m pip install -r requirements.txt
python3 -m fdroidmonitor -d
```
